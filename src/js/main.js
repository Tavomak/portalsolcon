$(document).ready(function () {
    var mq = window.matchMedia('(min-width: 768px)');
    var menuItems = $('.solc-product-feature').children().length;
    var menuPercentage = 100 / menuItems;
    var menuWdCalc = menuPercentage.toFixed(0) + '%';
    console.log(mq);

    //Remover el precargador una vez cargados todos los elementos de la pagina
    $(window).on('load', function () {
        $('.cd-loader').fadeOut('slow', function () {
            $(this).remove();
        });
    });

    //Funcion para que el submenu se muestre al hacer hover
    function hoverNav() {
        $('ul.nav .js-dropdown').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(100);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(100);
        });
    }

    if ($(window).width() >= 768) {
        hoverNav();
        $('.solc-product-feature-item').css('width', menuWdCalc);
    }
    //Evento que determina cuando se utiliza el submenu en hover
    $(window).resize(function () {
        mq = window.matchMedia('(min-width: 768px)');
        console.log(mq.matches);
        if (mq.matches) {
            hoverNav();
            $('.solc-product-feature-item').css('width', menuWdCalc);
        } else {
            $('.solc-product-feature-item').css('width', '100%');
        }
    }).resize();

    //Evento que activa la animacion del menu hamburguer
    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
    });

    //scroll reveal
    window.sr = ScrollReveal();
    sr.reveal('.js-rv', {
        origin: 'left',
        distance: '20px',
        duration: 500,
        delay: 500,
        reset: true
    });
    //sr.reveal('section');

});